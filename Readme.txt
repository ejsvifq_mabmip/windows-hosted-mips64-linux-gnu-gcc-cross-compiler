This is a canadian cross GCC toolchain (multilib). Built by the fast_io library's author

Unix Timestamp:1641455455.003108115
UTC:2022-01-06T07:50:55.003108115Z

fast_io:
https://github.com/tearosccebe/fast_io.git

build	:	x86_64-linux-gnu
host	:	x86_64-w64-mingw32
target	:	mips64-linux-gnu

mips64-linux-gnu-g++ -v
Using built-in specs.
COLLECT_GCC=mips64-linux-gnu-g++
COLLECT_LTO_WRAPPER=d:/x86_64-windows-gnu/mips64-linux-gnu/bin/../libexec/gcc/mips64-linux-gnu/12.0.0/lto-wrapper.exe
Target: mips64-linux-gnu
Configured with: ../../../../gcc/configure --disable-nls --disable-werror --disable-bootstrap --enable-languages=c,c++ --enable-multilib --with-pkgversion=cqwrteur --with-gxx-libcxx-include-dir=/home/cqwrteur/toolchains/x86_64-w64-mingw32-win32/mips64-linux-gnu/mips64-linux-gnu/include/c++/v1 --disable-libstdcxx-verbose --target=mips64-linux-gnu --prefix=/home/cqwrteur/toolchains/x86_64-w64-mingw32-win32/mips64-linux-gnu --host=x86_64-w64-mingw32
Thread model: posix
Supported LTO compression algorithms: zlib
gcc version 12.0.0 20220106 (experimental) (cqwrteur)
